#include <Windows.h>
#include <gl\GL.h>

class GLApplication
{
public:
	HDC			hDC;
	HGLRC		hRC;
	HWND		hWnd;
	HINSTANCE	hInstance;
	long		borderWidth;
	long		borderHeight;
	long		windowWidth;
	long		windowHeight;
	bool		isActive;
	bool		isFullscreen;

	DEVMODE		nativeMode;

	bool createWindow(LPCWSTR title, WNDPROC WndProc,
		long width, long height);
	void initGL(int bits);
	void destroyGLWindow();
	void destroyGL();
	void Render();
	void Resize(long w, long h);
};

void GLApplication::initGL(int bits)
{
	unsigned int		PixelFormat;
	static	PIXELFORMATDESCRIPTOR pfd;

	ZeroMemory(&pfd, sizeof(pfd));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = bits;
	pfd.cDepthBits = 16;
	pfd.iLayerType = PFD_MAIN_PLANE;

	hDC = GetDC(hWnd);
	PixelFormat = ChoosePixelFormat(hDC, &pfd);
	SetPixelFormat(hDC, PixelFormat, &pfd);
	hRC = wglCreateContext(hDC);
	wglMakeCurrent(hDC, hRC);
}

void GLApplication::destroyGLWindow()
{
	destroyGL();

	if (hDC)
	{
		ReleaseDC(hWnd, hDC);
		hDC = NULL;
	}

	if (hWnd)
	{
		DestroyWindow(hWnd);
		hWnd = NULL;
	}

	UnregisterClass(__TEXT("OGL"), hInstance);
	hInstance = NULL;
}

void GLApplication::destroyGL()
{
	if (isFullscreen)
	{
		ChangeDisplaySettings(NULL, 0);
		ShowCursor(true);
	}

	if (hRC)
	{
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(hRC);
		hRC = NULL;
	}
}

void GLApplication::Resize(long w, long h)
{
	windowWidth = w;
	windowHeight = h;
}

bool GLApplication::createWindow(LPCWSTR title, WNDPROC WndProc,
	long width, long height)
{
	WNDCLASS	wc;
	DWORD		dwExStyle;
	DWORD		dwStyle;
	RECT		rc;

	isActive		= true;
	isFullscreen	= false;

	EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &nativeMode);

	long x = nativeMode.dmPelsWidth / 2 - width / 2;
	long y = nativeMode.dmPelsHeight / 2 - height / 2;

	rc.left = (long) x;
	rc.right = (long) x + width;
	rc.top = (long) y;
	rc.bottom = (long) y + height;

	hInstance = GetModuleHandle(NULL);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = (WNDPROC)WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = NULL;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = __TEXT("OGL");

	RegisterClass(&wc);

	dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
	dwStyle = WS_OVERLAPPEDWINDOW | WS_MINIMIZEBOX | WS_CAPTION | WS_SYSMENU
		| WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

	AdjustWindowRectEx(&rc, dwStyle, false, dwExStyle);

	hWnd = CreateWindowEx(dwExStyle, __TEXT("OGL"), title, dwStyle,
		rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top,
		NULL, NULL, hInstance, NULL);

	RECT rcClient, rcWind;
	GetClientRect(hWnd, &rcClient);
	GetWindowRect(hWnd, &rcWind);
	borderWidth = 2 * ((rcWind.right - rcWind.left) - rcClient.right) / 2;
	borderHeight = 2 * ((rcWind.bottom - rcWind.top) - rcClient.bottom) / 2;

	initGL(32);

	SetForegroundWindow(hWnd);
	SetFocus(hWnd);
	ShowWindow(hWnd, SW_SHOW);
	return true;
}

void DrawQuad(float x, float y, float w, float h)
{
	glBegin(GL_QUADS);
	glColor4f(0.5f, 0.5f, 0.5f, 1.0f); glTexCoord2d(0, 1);	glVertex2f(x, y);
	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); glTexCoord2d(1, 1);	glVertex2f(x + w, y);
	glColor4f(1.0f, 0.0f, 0.0f, 1.0f); glTexCoord2d(1, 0);	glVertex2f(x + w, y + h);
	glColor4f(0.0f, 1.0f, 0.0f, 1.0f); glTexCoord2d(0, 0);	glVertex2f(x, y + h);
	glEnd();
}

void GLApplication::Render()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glViewport(0, 0, windowWidth, windowHeight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, 1, 0, 1, -1, 1);
	
	glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	DrawQuad(0, 0, 1, 1);

	glFlush();
	SwapBuffers(hDC);
}

GLApplication app;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{

	case WM_ACTIVATE:
	{
		KillTimer(hWnd, 0);
		if (!HIWORD(wParam))
			app.isActive = true;
		else
			app.isActive = false;
	}
	return 0;

	case WM_GETMINMAXINFO:
		((MINMAXINFO *)lParam)->ptMinTrackSize.x = 0 + app.borderWidth;
		((MINMAXINFO *)lParam)->ptMinTrackSize.y = 0 + app.borderHeight;
		return 0;

	case WM_NCLBUTTONDOWN:
		SetTimer(hWnd, NULL, 1, NULL);
		return DefWindowProc(hWnd, message, wParam, lParam);

	case WM_NCLBUTTONUP:
		KillTimer(hWnd, 0);
		return 0;

	case WM_TIMER:
		app.Render();
		return 0;

	case WM_EXITSIZEMOVE:
		KillTimer(hWnd, 0);
		return 0;

	case WM_CLOSE:
		PostQuitMessage(0);
		return 0;

	case WM_SIZE:
		app.Resize(LOWORD(lParam), HIWORD(lParam));
		return 0;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);

	}
}

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	MSG msg;
	bool quit = false;

	app.createWindow(__TEXT("gl-window"), WndProc, 320, 240);
	
	while (!quit)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				quit = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			//runloop
			app.Render();
		}
	}

	//cleanup
	app.destroyGLWindow();
	return msg.wParam;
}
